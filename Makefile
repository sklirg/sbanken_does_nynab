.PHONY: version

version:
	@if [ -z "${VERSION}" ]; then echo "Missing VERSION argument" && exit 1; fi
	sed -i'' -e "s/[0-9]\.[0-9]\.[0-9]-WIP/${VERSION}/" Cargo.toml
