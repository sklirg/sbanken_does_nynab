extern crate serde;
extern crate serde_derive;
extern crate serde_json;

use std::convert::From;

use self::serde::de::Deserializer;
use self::serde::Deserialize;

use crate::ynab::helpers::to_milliunits;
use rustbanken::model::ArchiveTransaction as SbankenTransaction;

#[derive(Clone, Deserialize)]
pub struct YnabConfig {
    pub access_token: String,
    pub budget_id: String,

    pub accounts: Vec<AccountMapping>,
}

#[derive(Clone, Debug, Deserialize)]
pub struct AccountMapping {
    pub name: String,
    pub sbanken_id: String,
    pub ynab_id: String,
}

#[derive(Debug, Deserialize)]
pub struct Budget {
    pub id: String,
    pub name: String,
    pub last_modified_on: String,
    pub first_month: String,
    pub last_month: String,
}

#[derive(Deserialize)]
pub struct BudgetBaseResponse {
    pub data: BudgetResponse,
}

#[derive(Deserialize)]
pub struct BudgetResponse {
    pub budgets: Vec<Budget>,
}

#[derive(Debug, Deserialize)]
pub struct Account {
    pub id: String,
    pub name: String,

    #[serde(rename = "type")]
    pub account_type: String,

    pub on_budget: bool,
    pub closed: bool,

    #[serde(deserialize_with = "null_to_empty_string")]
    pub note: String,
    pub balance: i32,
    pub cleared_balance: i32,
    pub uncleared_balance: i32,
    pub transfer_payee_id: String,
    pub deleted: bool,
}

#[derive(Deserialize)]
pub struct AccountResponse {
    pub accounts: Vec<Account>,
}

#[derive(Deserialize)]
pub struct AccountBaseResponse {
    pub data: AccountResponse,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Transaction {
    pub account_id: String,
    pub date: String,
    pub payee_name: Option<String>,
    pub amount: i32,
    pub memo: Option<String>,
    pub import_id: String,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct TransactionsRequest {
    pub transactions: Vec<Transaction>,
}

fn null_to_empty_string<'de, D>(d: D) -> Result<String, D::Error>
where
    D: Deserializer<'de>,
{
    Deserialize::deserialize(d).map(|x: Option<_>| x.unwrap_or("".to_string()))
}

impl From<&SbankenTransaction> for Transaction {
    fn from(t: &SbankenTransaction) -> Self {
        Transaction {
            account_id: "".to_string(),
            amount: to_milliunits(&t.amount),
            date: t.accounting_date.to_string(),
            import_id: format!("sb:{}", t.transaction_id.clone()),
            payee_name: Some(t.text.to_string()),
            memo: Some("".to_string()),
        }
    }
}
