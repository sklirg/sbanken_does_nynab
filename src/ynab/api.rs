use crate::ynab::helpers::build_api_client;
use crate::ynab::model::{TransactionsRequest, YnabConfig};
use reqwest::StatusCode;
use rustbanken::model::ArchiveTransaction;
use std::collections::HashMap;

use crate::ynab::model::Transaction;

use std::io::Read;

const BUDGETS_API: &str = "https://api.youneedabudget.com/v1/budgets";
// const TRANSACTION_API: &str = "https://api.youneedabudget.com/v1/budgets/{}/transactions";

pub fn post_transactions(config: &YnabConfig, transactions: Vec<Transaction>) {
    let fmt_url = format!("{}/{}/transactions", BUDGETS_API, config.budget_id);

    let transaction_request_body = TransactionsRequest { transactions };

    let body =
        do_api_post_transaction_request(config, parse_url(&fmt_url), transaction_request_body);

    trace!("Posted transactions {:#?}", body);
}

fn parse_url(url: &str) -> reqwest::Url {
    return match reqwest::Url::parse(url) {
        Ok(val) => val,
        Err(error) => panic!("Failed to parse url: {}", error),
    };
}

fn do_api_post_transaction_request(
    config: &YnabConfig,
    url: reqwest::Url,
    body: TransactionsRequest,
) -> String {
    let client = build_api_client(config);

    let request = client.post(url).json(&body).send();

    let mut resp = match request {
        Ok(resp) => resp,
        Err(error) => {
            error!("{}", error);
            panic!("Response failed");
        }
    };

    match resp.status() {
        StatusCode::OK | StatusCode::CREATED => trace!("HTTP OK"),
        status => warn!("Unhandled status code: {} ({:#?})", status, resp),
    }

    let mut body = String::new();
    match resp.read_to_string(&mut body) {
        Ok(data) => {
            trace!("Receieved {} data ({})", data, body);
        }
        Err(error) => {
            error!("Failed to read response to string: {}", error);
        }
    }

    return body;
}

pub fn update_ynab(config: &YnabConfig, accounts: &HashMap<String, Vec<ArchiveTransaction>>) {
    let mut transactions: Vec<Transaction> = Vec::new();

    for account in accounts.keys() {
        let account_transactions: &Vec<ArchiveTransaction> = match accounts.get(account) {
            Some(t) => t,
            None => panic!("Getting transactions from hashmap failed"),
        };

        debug!("Doing things to {}", account);

        let ynab_account_id =
            match get_ynab_account_id_from_sbanken_account_id(config, account.to_string()) {
                Some(id) => id,
                None => {
                    warn!("missing account id mapping for account '{}'", account);
                    continue;
                }
            };

        // Sbanken decided to drop transactionId
        // filter(|t| t.transaction_id != "0" && t.transaction_id != "")
        for transaction in account_transactions.into_iter() {
            let mut converted_transaction = Transaction::from(transaction);
            converted_transaction.account_id = ynab_account_id.clone();

            trace!("Converted transaction {:#?}", converted_transaction);

            transactions.push(converted_transaction);
        }
    }

    if transactions.is_empty() {
        warn!("transactions list is empty!");
        return;
    }

    info!("Posting {} transactions to YNAB", transactions.len());

    post_transactions(config, transactions)
}

fn get_ynab_account_id_from_sbanken_account_id(
    config: &YnabConfig,
    sbanken_account_id: String,
) -> Option<String> {
    for acc in &config.accounts {
        if acc.sbanken_id == sbanken_account_id {
            return Some(acc.ynab_id.clone());
        }
    }
    None
}
