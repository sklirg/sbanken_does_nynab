#[macro_use]
extern crate log;
extern crate log4rs;
extern crate reqwest;
extern crate rustbanken;

#[macro_use]
extern crate serde_derive;

mod config;
mod helpers;
mod traits;
mod ynab;

use crate::config::config::configure_from_file;
use rustbanken::model::ArchiveTransaction;
use std::collections::HashMap;
use traits::{TransactionFetcher, TransactionPusher};

fn main() {
    log4rs::init_file("log4rs.yml", Default::default()).unwrap();

    info!("Starting app");

    let mut all_transactions: Option<HashMap<String, Vec<ArchiveTransaction>>> = None;

    let path = "config.toml";
    let config = match configure_from_file(&path) {
        Ok(conf) => conf,
        Err(config_error) => {
            error!(
                "failed to configure application, got configuration error: {:?}",
                config_error
            );
            return;
        }
    };

    if config.get_fetchers().is_empty() {
        error!("no fetchers are configured, exiting");
        return;
    }

    for fetcher in config.get_fetchers() {
        info!("Running '{}' fetcher", fetcher);

        let doer = match fetcher {
            config::config::Fetcher::SbankenFetcher(x) => x,
        };
        let transactions = doer.fetch_transactions();

        // @ToDo: append instead of overwriting
        all_transactions = transactions;

        // @ToDo: use "pushers" config instead of this two-step process
    }

    if let None = all_transactions {
        warn!("no transactions");
        return;
    };

    for pusher in config.get_pushers() {
        info!("running '{}' pusher", pusher);

        let tx = all_transactions.clone().unwrap();

        let doer = match pusher {
            config::config::Pusher::YnabPusher(x) => x,
        };
        doer.push_transactions(&tx);
    }

    info!("Done.");
}
