use std::fs;
use toml;

use crate::config::sbanken::SbankenFetcher;
use crate::config::ynab::YnabPusher;

#[derive(Debug)]
pub enum ConfigError {
    ConfigParseError(String),
    IoError(String),
}

#[derive(Deserialize)]
pub struct AppConfig {
    pub fetchers: ConfiguredFetchersConfig,
    pub pushers: ConfiguredPushersConfig,
}

impl AppConfig {
    pub fn get_fetchers(&self) -> Vec<Fetcher> {
        let mut fetchers: Vec<Fetcher> = Vec::new();

        for fetcher in &self.fetchers.sbanken {
            let fetcher_name = match &fetcher.name {
                Some(name) => name,
                None => "sbanken",
            };
            let doer = Fetcher::SbankenFetcher(SbankenFetcher {
                name: Some(fetcher_name.to_string()),
                config: rustbanken::SbankenConfig {
                    username: fetcher.config.username.clone(),
                    password: fetcher.config.password.clone(),
                },
            });

            fetchers.push(doer);
        }

        fetchers
    }

    pub fn get_pushers(&self) -> Vec<Pusher> {
        let mut pushers: Vec<Pusher> = Vec::new();

        for pusher in &self.pushers.ynab {
            let pusher_name = match &pusher.name {
                Some(name) => name,
                None => "ynab",
            };
            let doer = Pusher::YnabPusher(YnabPusher {
                name: Some(pusher_name.to_string()),
                config: crate::ynab::model::YnabConfig {
                    access_token: pusher.config.access_token.clone(),
                    budget_id: pusher.config.budget_id.clone(),
                    accounts: pusher.config.accounts.clone(),
                },
            });

            pushers.push(doer);
        }

        pushers
    }
}

#[derive(Deserialize)]
pub struct ConfiguredFetchersConfig {
    pub sbanken: Vec<SbankenFetcher>,
}
#[derive(Deserialize)]
pub struct ConfiguredPushersConfig {
    pub ynab: Vec<YnabPusher>,
}

#[derive(Deserialize)]
pub enum Fetcher {
    SbankenFetcher(SbankenFetcher),
}

impl Fetcher {
    fn get_type(&self) -> &str {
        match &self {
            Fetcher::SbankenFetcher(_) => "sbanken",
        }
    }
}
impl std::fmt::Display for Fetcher {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        f.write_str(self.get_type())
    }
}

#[derive(Deserialize)]
pub struct FetcherConf {
    pub name: String, // the name of the fetcher 'type' we use
    pub doer: Fetcher,
}

#[derive(Deserialize)]
pub enum Pusher {
    YnabPusher(YnabPusher),
}
impl Pusher {
    fn get_type(&self) -> &str {
        match &self {
            Pusher::YnabPusher(_) => "ynab",
        }
    }
}
impl std::fmt::Display for Pusher {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> Result<(), std::fmt::Error> {
        f.write_str(self.get_type())
    }
}

#[derive(Deserialize)]
pub struct PusherConf {
    pub name: String, // the name of the pusher 'type' we use
    pub doer: Pusher,
}

pub fn configure_from_file(path: &str) -> Result<AppConfig, ConfigError> {
    let contents = match fs::read_to_string(path) {
        Ok(b) => b,
        Err(e) => return Err(ConfigError::IoError(e.to_string())),
    };

    match toml::from_str(&contents) {
        Ok(conf) => Ok(conf),
        Err(err) => Err(ConfigError::ConfigParseError(err.to_string())),
    }
}
