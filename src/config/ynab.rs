use std::collections::HashMap;

use rustbanken::model::ArchiveTransaction;

use crate::traits::TransactionPusher;
use crate::ynab::api::update_ynab;
use crate::ynab::model::YnabConfig;

#[derive(Clone, Deserialize)]
pub struct YnabPusher {
    pub name: Option<String>,
    pub config: YnabConfig,
}

impl TransactionPusher for YnabPusher {
    fn push_transactions(&self, transactions: &HashMap<String, Vec<ArchiveTransaction>>) {
        update_ynab(&self.config, transactions)
    }
}
