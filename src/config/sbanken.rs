use std::collections::HashMap;

use rustbanken::model::{ArchiveTransaction, SbankenConfig};

use crate::traits::TransactionFetcher;

#[derive(Clone, Deserialize)]
pub struct SbankenFetcher {
    pub name: Option<String>,
    pub config: SbankenConfig,
}
impl TransactionFetcher for SbankenFetcher {
    fn fetch_transactions(&self) -> Option<HashMap<String, Vec<ArchiveTransaction>>> {
        rustbanken::api::fetch_transactions_from_sbanken(&self.config)
    }
}
