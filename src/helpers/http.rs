use reqwest::header;

pub enum AuthenticationType {
    Bearer,
}

pub fn generate_auth_header(
    credentials: String,
    auth_type: AuthenticationType,
) -> header::HeaderValue {
    let auth_header = match auth_type {
        AuthenticationType::Bearer => format!("Bearer {}", credentials),
    };

    return generate_header_value(&auth_header);
}

fn generate_header_value(value: &str) -> header::HeaderValue {
    return match header::HeaderValue::from_str(&value) {
        Ok(val) => val,
        Err(error) => {
            panic!("Failed to generate HTTP header: {}", error);
        }
    };
}
