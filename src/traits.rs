use rustbanken::model::ArchiveTransaction;
use std::collections::HashMap;

pub trait TransactionFetcher {
    fn fetch_transactions(&self) -> Option<HashMap<String, Vec<ArchiveTransaction>>>;
}

pub trait TransactionPusher {
    fn push_transactions(&self, transactions: &HashMap<String, Vec<ArchiveTransaction>>) -> (); //Result<t, e>;
}
